# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: gedit-collaboration\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-01-16 17:21+0100\n"
"PO-Revision-Date: 2011-01-15 20:42+0100\n"
"Last-Translator: marcoslans <marcoslansgarza@gmail.com>\n"
"Language-Team: Galician <gnome@g11n.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Poedit-Language: Galician\n"

#: ../data/org.gnome.gedit.plugins.collaboration.gschema.xml.in.in.h:1
msgid "Default User Hue"
msgstr "Ton predefinido do usuario"

#: ../data/org.gnome.gedit.plugins.collaboration.gschema.xml.in.in.h:2
msgid "Default User Name"
msgstr "Nome de usuario predefinido"

#: ../data/org.gnome.gedit.plugins.collaboration.gschema.xml.in.in.h:3
msgid "The default user hue part of the color to identify the user by."
msgstr ""
"O ton predefinido do usuario forma parte da cor pola que se identifica ao "
"usuario."

#: ../data/org.gnome.gedit.plugins.collaboration.gschema.xml.in.in.h:4
msgid "The default user name for collaboration"
msgstr "Nome de usuario predefinido para a colaboración"

#: ../src/collaboration.plugin.desktop.in.in.h:1
#: ../src/gedit-collaboration-window-helper.c:1629
msgid "Collaboration"
msgstr "Colaboración"

#: ../src/collaboration.plugin.desktop.in.in.h:2
msgid "Collaboration plugin"
msgstr "Engadido para colaboración"

#: ../src/gedit-collaboration-actions.c:104
msgid "New File"
msgstr "Ficheiro novo"

#: ../src/gedit-collaboration-actions.c:104
msgid "New Folder"
msgstr "Cartafol novo"

#: ../src/gedit-collaboration-actions.c:116
msgid "File _name:"
msgstr "Nome do _ficheiro:"

#: ../src/gedit-collaboration-actions.c:116
msgid "Folder _name:"
msgstr "_Nome do cartafol:"

#: ../src/gedit-collaboration-bookmark-dialog.c:249
msgid "Defaults"
msgstr "Predefinicións"

#: ../src/gedit-collaboration-bookmark-dialog.c:262
msgid "Create New Bookmark"
msgstr "Crear un marcador novo"

#: ../src/gedit-collaboration-bookmark-dialog.c:274
msgid "Bookmark Properties"
msgstr "Propiedades do marcador"

#: ../src/gedit-collaboration-color-button.c:71
msgid "Select User Color"
msgstr "Seleccionar a cor do usuario"

#: ../src/gedit-collaboration-document-message.c:128
msgid "The collaboration session for this file was closed"
msgstr "Pechouse a sesión de colaboración deste ficheiro"

#: ../src/gedit-collaboration-document-message.c:151
msgid "State vector has a bad format"
msgstr "O vector de estado ten un formato incorrecto"

#: ../src/gedit-collaboration-document-message.c:154
msgid "State vector failed"
msgstr "Produciuse un fallo no vector de estado"

#: ../src/gedit-collaboration-document-message.c:165
msgid "Adopted session user does not exist"
msgstr "O nome de usuario non existe na sesión adoptada"

#: ../src/gedit-collaboration-document-message.c:168
msgid "Adopted session missing operation"
msgstr "Falta a operación na sesión adoptada"

#: ../src/gedit-collaboration-document-message.c:171
msgid "Adopted session invalid request"
msgstr "Solicitude incorrecta na sesión adoptada"

#: ../src/gedit-collaboration-document-message.c:174
msgid "Adopted session missing state vector"
msgstr "Falta o vector de estado na sesión adoptada"

#: ../src/gedit-collaboration-document-message.c:177
msgid "Adopted session failed"
msgstr "Produciuse un fallo na sesión adoptada"

#: ../src/gedit-collaboration-document-message.c:184
msgid "An unknown error occurred"
msgstr "Ocorreu un erro descoñecido"

#: ../src/gedit-collaboration-manager.c:738
msgid "Synchronizing document"
msgstr "Sincronizando o documento"

#: ../src/gedit-collaboration-manager.c:739
msgid "Please wait while the shared document is being synchronized"
msgstr "Agarde mentres se sincroniza o documento compartido"

#. Translators: "Chat" is the label (noun) of the bottom pane in which
#. the collaboration chat embedded and is used as a fallback label when
#. the session name could not be determined
#: ../src/gedit-collaboration-window-helper.c:720
msgid "Chat"
msgstr "Conversar"

#: ../src/gedit-collaboration-window-helper.c:992
#, c-format
msgid "Please provide a password for %s"
msgstr "Indique un contrasinal para %s"

#: ../src/gedit-collaboration-window-helper.c:1000
msgid "Note: The connection is not secure"
msgstr "Aviso: a conexión non é segura"

#: ../src/gedit-collaboration-window-helper.c:1501
msgid "Clear _Collaboration Colors"
msgstr "Limpar as _cores de colaboración"

#: ../src/gedit-collaboration-window-helper.c:1502
msgid "Clear collaboration user colors"
msgstr "Limpar as cores de colaboración do usuario"
