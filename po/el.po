# Greek translation for gedit-collaboration.
# Copyright (C) 2011 gedit-collaboration's COPYRIGHT HOLDER
# This file is distributed under the same license as the gedit-collaboration package.
# Evangelos Vafeiadis <vagvaf@gmail.com>, 2011.
# Evangelos Vafeiadis <vagvaf@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: gedit-collaboration master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=gedit&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2011-05-03 00:06+0000\n"
"PO-Revision-Date: 2011-05-07 22:21+0200\n"
"Last-Translator: Evangelos Vafeiadis <vagvaf@gmail.com>\n"
"Language-Team: Greek <team@gnome.gr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/org.gnome.gedit.plugins.collaboration.gschema.xml.in.in.h:1
msgid "Default User Hue"
msgstr "Προεπιλέγμενη απόχρωση χρήστη"

#: ../data/org.gnome.gedit.plugins.collaboration.gschema.xml.in.in.h:2
msgid "Default User Name"
msgstr "Προεπιλεγμένο όνομα χρήστη"

#: ../data/org.gnome.gedit.plugins.collaboration.gschema.xml.in.in.h:3
msgid "The default user hue part of the color to identify the user by."
msgstr ""
"Η προεπιλεγμένη απόχρωση χρήστη είναι μέρος του χρώματος που συμβάλλει στην "
"αναγνώριση του."

#: ../data/org.gnome.gedit.plugins.collaboration.gschema.xml.in.in.h:4
msgid "The default user name for collaboration"
msgstr "Το προεπιλεγμενό όνομα χρήστη για συνεργασία"

#: ../src/collaboration.plugin.desktop.in.in.h:1
#: ../src/gedit-collaboration-window-helper.c:1631
msgid "Collaboration"
msgstr "Συνεργασία"

#: ../src/collaboration.plugin.desktop.in.in.h:2
msgid "Collaboration plugin"
msgstr "Πρόσθετο συνεργασίας"

#: ../src/gedit-collaboration-actions.c:103
msgid "New File"
msgstr "Νέο αρχείο"

#: ../src/gedit-collaboration-actions.c:103
msgid "New Folder"
msgstr "Νέος φάκελος"

#: ../src/gedit-collaboration-actions.c:115
msgid "File _name:"
msgstr "Όνομα αρ_χείου:"

#: ../src/gedit-collaboration-actions.c:115
msgid "Folder _name:"
msgstr "Ό_νομα φακέλου:"

#: ../src/gedit-collaboration-bookmark-dialog.c:249
msgid "Defaults"
msgstr "Προεπιλογές"

#: ../src/gedit-collaboration-bookmark-dialog.c:262
msgid "Create New Bookmark"
msgstr "Δημιουργία νέου σελιδοδείκτη"

#: ../src/gedit-collaboration-bookmark-dialog.c:274
msgid "Bookmark Properties"
msgstr "Ιδιότητες σελιδοδείκτη"

#: ../src/gedit-collaboration-color-button.c:71
msgid "Select User Color"
msgstr "Επιλέξτε το χρώμα του χρήστη"

#: ../src/gedit-collaboration-document-message.c:128
msgid "The collaboration session for this file was closed"
msgstr "Τερματίστηκε η συνεδρία συνεργασίας για αυτό το έγγραφο"

#: ../src/gedit-collaboration-document-message.c:151
msgid "State vector has a bad format"
msgstr "Ο δείκτης κατάστασης είναι κακώς μορφοποιημένος"

#: ../src/gedit-collaboration-document-message.c:154
msgid "State vector failed"
msgstr "Αποτυχία του δείκτη κατάστασης"

#: ../src/gedit-collaboration-document-message.c:165
msgid "Adopted session user does not exist"
msgstr "Δεν υπάρχει ο χρήστης της αναληφθείσας συνεδρίας"

#: ../src/gedit-collaboration-document-message.c:168
msgid "Adopted session missing operation"
msgstr "Απουσιάζει η λειτουργία από την αναληφθείσα συνεδρία."

#: ../src/gedit-collaboration-document-message.c:171
msgid "Adopted session invalid request"
msgstr ""
"Λήφθηκε άκυρη τιμή στην αίτηση καταχώρησης για την αναληφθείσα συνεδρία"

#: ../src/gedit-collaboration-document-message.c:174
msgid "Adopted session missing state vector"
msgstr "Απουσιάζει ο δείκτης κατάστασης της αναληφθείσας συνεδρίας."

#: ../src/gedit-collaboration-document-message.c:177
msgid "Adopted session failed"
msgstr "Απέτυχε η αναληφθείσα συνεδρία"

#: ../src/gedit-collaboration-document-message.c:184
msgid "An unknown error occurred"
msgstr "Συνέβη ένα άγνωστο σφάλμα"

#: ../src/gedit-collaboration-manager.c:790
msgid "Synchronizing document"
msgstr "Συγχρονισμός εγγράφου"

#: ../src/gedit-collaboration-manager.c:791
msgid "Please wait while the shared document is being synchronized"
msgstr "Παρακαλώ περιμενέτε όσο το κοινό έγγραφο συγχρονίζεται."

#. Translators: "Chat" is the label (noun) of the bottom pane in which
#. the collaboration chat embedded and is used as a fallback label when
#. the session name could not be determined
#: ../src/gedit-collaboration-window-helper.c:722
msgid "Chat"
msgstr "Συνομιλία"

#: ../src/gedit-collaboration-window-helper.c:994
#, c-format
msgid "Please provide a password for %s"
msgstr "Επιλέξτε ένα κωδικό για το %s"

#: ../src/gedit-collaboration-window-helper.c:1002
msgid "Note: The connection is not secure"
msgstr "Σημείωση: Η σύνδεση δεν είναι ασφαλής"

#: ../src/gedit-collaboration-window-helper.c:1503
msgid "Clear _Collaboration Colors"
msgstr "_Καθαρισμός χρωμάτων συνεργασίας"

#: ../src/gedit-collaboration-window-helper.c:1504
msgid "Clear collaboration user colors"
msgstr "Καθαρισμός χρωμάτων των συνεργαζόμενων χρηστών"
