/*
 * Copyright (C) 2010 - Jesse van den Kieboom
 *
 * gedit-collaboration is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gedit-collaboration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gedit-collaboration. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GEDIT_COLLABORATION_WINDOW_HELPER_H__
#define __GEDIT_COLLABORATION_WINDOW_HELPER_H__

#include <glib-object.h>
#include <gedit/gedit-window.h>
#include <libpeas/peas-extension-base.h>

G_BEGIN_DECLS

#define GEDIT_TYPE_COLLABORATION_WINDOW_HELPER			(gedit_collaboration_window_helper_get_type ())
#define GEDIT_COLLABORATION_WINDOW_HELPER(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_TYPE_COLLABORATION_WINDOW_HELPER, GeditCollaborationWindowHelper))
#define GEDIT_COLLABORATION_WINDOW_HELPER_CONST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_TYPE_COLLABORATION_WINDOW_HELPER, GeditCollaborationWindowHelper const))
#define GEDIT_COLLABORATION_WINDOW_HELPER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GEDIT_TYPE_COLLABORATION_WINDOW_HELPER, GeditCollaborationWindowHelperClass))
#define GEDIT_IS_COLLABORATION_WINDOW_HELPER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEDIT_TYPE_COLLABORATION_WINDOW_HELPER))
#define GEDIT_IS_COLLABORATION_WINDOW_HELPER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GEDIT_TYPE_COLLABORATION_WINDOW_HELPER))
#define GEDIT_COLLABORATION_WINDOW_HELPER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GEDIT_TYPE_COLLABORATION_WINDOW_HELPER, GeditCollaborationWindowHelperClass))

typedef struct _GeditCollaborationWindowHelper		GeditCollaborationWindowHelper;
typedef struct _GeditCollaborationWindowHelperClass	GeditCollaborationWindowHelperClass;
typedef struct _GeditCollaborationWindowHelperPrivate	GeditCollaborationWindowHelperPrivate;

struct _GeditCollaborationWindowHelper
{
	PeasExtensionBase parent;

	GeditCollaborationWindowHelperPrivate *priv;
};

struct _GeditCollaborationWindowHelperClass
{
	PeasExtensionBaseClass parent_class;
};

GType gedit_collaboration_window_helper_get_type (void) G_GNUC_CONST;

void _gedit_collaboration_window_helper_register_type (GTypeModule *module);

G_END_DECLS

#endif /* __GEDIT_COLLABORATION_WINDOW_HELPER_H__ */
