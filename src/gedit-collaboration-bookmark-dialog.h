/*
 * Copyright (C) 2010 - Jesse van den Kieboom
 *
 * gedit-collaboration is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gedit-collaboration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gedit-collaboration. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GEDIT_COLLABORATION_BOOKMARK_DIALOG_H__
#define __GEDIT_COLLABORATION_BOOKMARK_DIALOG_H__

#include <gtk/gtk.h>
#include "gedit-collaboration-bookmarks.h"

G_BEGIN_DECLS

#define GEDIT_COLLABORATION_TYPE_BOOKMARK_DIALOG		(gedit_collaboration_bookmark_dialog_get_type ())
#define GEDIT_COLLABORATION_BOOKMARK_DIALOG(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_COLLABORATION_TYPE_BOOKMARK_DIALOG, GeditCollaborationBookmarkDialog))
#define GEDIT_COLLABORATION_BOOKMARK_DIALOG_CONST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_COLLABORATION_TYPE_BOOKMARK_DIALOG, GeditCollaborationBookmarkDialog const))
#define GEDIT_COLLABORATION_BOOKMARK_DIALOG_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GEDIT_COLLABORATION_TYPE_BOOKMARK_DIALOG, GeditCollaborationBookmarkDialogClass))
#define GEDIT_COLLABORATION_IS_BOOKMARK_DIALOG(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEDIT_COLLABORATION_TYPE_BOOKMARK_DIALOG))
#define GEDIT_COLLABORATION_IS_BOOKMARK_DIALOG_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GEDIT_COLLABORATION_TYPE_BOOKMARK_DIALOG))
#define GEDIT_COLLABORATION_BOOKMARK_DIALOG_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GEDIT_COLLABORATION_TYPE_BOOKMARK_DIALOG, GeditCollaborationBookmarkDialogClass))

typedef struct _GeditCollaborationBookmarkDialog	GeditCollaborationBookmarkDialog;
typedef struct _GeditCollaborationBookmarkDialogClass	GeditCollaborationBookmarkDialogClass;
typedef struct _GeditCollaborationBookmarkDialogPrivate	GeditCollaborationBookmarkDialogPrivate;

struct _GeditCollaborationBookmarkDialog
{
	GtkDialog parent;

	GeditCollaborationBookmarkDialogPrivate *priv;
};

struct _GeditCollaborationBookmarkDialogClass
{
	GtkDialogClass parent_class;
};

GType gedit_collaboration_bookmark_dialog_get_type (void) G_GNUC_CONST;
void _gedit_collaboration_bookmark_dialog_register_type (GTypeModule *module);

GtkWidget	*gedit_collaboration_bookmark_dialog_new (GeditCollaborationBookmark *bookmark);

G_END_DECLS

#endif /* __GEDIT_COLLABORATION_BOOKMARK_DIALOG_H__ */
