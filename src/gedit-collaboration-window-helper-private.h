/*
 * Copyright (C) 2010 - Jesse van den Kieboom
 *
 * gedit-collaboration is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gedit-collaboration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gedit-collaboration. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GEDIT_COLLABORATION_WINDOW_HELPER_PRIVATE_H__
#define __GEDIT_COLLABORATION_WINDOW_HELPER_PRIVATE_H__

#include <config.h>
#include <glib/gi18n-lib.h>
#include <gedit/gedit-window.h>

#include <libinfgtk/inf-gtk-browser-view.h>
#include <libinfgtk/inf-gtk-browser-store.h>
#include <libinfgtk/inf-gtk-io.h>
#include <libinfinity/common/inf-xmpp-manager.h>
#include <libinfinity/inf-config.h>

#ifdef LIBINFINITY_HAVE_AVAHI
#include <libinfinity/common/inf-discovery-avahi.h>
#endif

#include "gedit-collaboration-manager.h"

#define BOOKMARK_DATA_KEY "GeditCollaborationBookmarkDataKey"

G_BEGIN_DECLS

struct _GeditCollaborationWindowHelperPrivate
{
	GeditWindow *window;

	InfIo *io;
	InfCertificateCredentials *certificate_credentials;
	InfGtkBrowserStore *browser_store;
	GtkWidget *browser_view;
	GeditCollaborationManager *manager;

	guint added_handler_id;
	guint removed_handler_id;

	GtkBuilder *builder;
	GtkUIManager *uimanager;
	GtkWidget *panel_widget;

	guint ui_id;
	GtkActionGroup *action_group;

	guint active_tab_changed_handler_id;
	GtkWidget *scrolled_window_user_view;
	GtkWidget *tree_view_user_view;
};

G_END_DECLS

#endif /* __GEDIT_COLLABORATION_WINDOW_HELPER_PRIVATE_H__ */

