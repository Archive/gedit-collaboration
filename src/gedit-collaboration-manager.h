/*
 * Copyright (C) 2010 - Jesse van den Kieboom
 *
 * gedit-collaboration is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gedit-collaboration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gedit-collaboration. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GEDIT_COLLABORATION_MANAGER_H__
#define __GEDIT_COLLABORATION_MANAGER_H__

#include <glib-object.h>
#include <gedit/gedit-window.h>
#include <libinfinity/client/infc-note-plugin.h>
#include <libinfinity/client/infc-browser.h>
#include "gedit-collaboration-user.h"
#include "gedit-collaboration-user-store.h"

G_BEGIN_DECLS

#define GEDIT_COLLABORATION_TYPE_MANAGER			(gedit_collaboration_manager_get_type ())
#define GEDIT_COLLABORATION_MANAGER(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_COLLABORATION_TYPE_MANAGER, GeditCollaborationManager))
#define GEDIT_COLLABORATION_MANAGER_CONST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_COLLABORATION_TYPE_MANAGER, GeditCollaborationManager const))
#define GEDIT_COLLABORATION_MANAGER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GEDIT_COLLABORATION_TYPE_MANAGER, GeditCollaborationManagerClass))
#define GEDIT_COLLABORATION_IS_MANAGER(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEDIT_COLLABORATION_TYPE_MANAGER))
#define GEDIT_COLLABORATION_IS_MANAGER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GEDIT_COLLABORATION_TYPE_MANAGER))
#define GEDIT_COLLABORATION_MANAGER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GEDIT_COLLABORATION_TYPE_MANAGER, GeditCollaborationManagerClass))

typedef struct _GeditCollaborationManager		GeditCollaborationManager;
typedef struct _GeditCollaborationManagerClass		GeditCollaborationManagerClass;
typedef struct _GeditCollaborationManagerPrivate	GeditCollaborationManagerPrivate;

typedef struct _GeditCollaborationSubscription		GeditCollaborationSubscription;

struct _GeditCollaborationManager
{
	GObject parent;

	GeditCollaborationManagerPrivate *priv;
};

struct _GeditCollaborationManagerClass
{
	GObjectClass parent_class;
};

GType gedit_collaboration_manager_get_type (void) G_GNUC_CONST;
void _gedit_collaboration_manager_register_type (GTypeModule *type_module);

GeditCollaborationManager *gedit_collaboration_manager_new (GeditWindow *window,
                                                            InfIo       *io);

InfcNotePlugin *gedit_collaboration_manager_get_note_plugin (GeditCollaborationManager *manager);
InfcNodeRequest *gedit_collaboration_manager_subscribe (GeditCollaborationManager *manager,
                                                        GeditCollaborationUser    *user,
                                                        InfcBrowser               *browser,
                                                        const InfcBrowserIter     *iter);

void gedit_collaboration_manager_clear_colors (GeditCollaborationManager *manager,
                                               GeditTab                  *tab);

GeditCollaborationSubscription *
gedit_collaboration_manager_tab_get_subscription (GeditCollaborationManager *manager,
                                                  GeditTab                  *tab);

GeditCollaborationUserStore *
gedit_collaboration_subscription_get_user_store (GeditCollaborationSubscription *subscription);

G_END_DECLS

#endif /* __GEDIT_COLLABORATION_MANAGER_H__ */
