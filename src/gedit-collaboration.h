/*
 * Copyright (C) 2010 - Jesse van den Kieboom
 *
 * gedit-collaboration is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gedit-collaboration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gedit-collaboration. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GEDIT_COLLABORATION_H__
#define __GEDIT_COLLABORATION_H__

#include <gtk/gtk.h>
#include <libinfinity/common/inf-protocol.h>

#define DEFAULT_INFINOTE_PORT (inf_protocol_get_default_port ())

G_BEGIN_DECLS

#define GEDIT_COLLABORATION_ERROR gedit_collaboration_error_quark ()

enum
{
	GEDIT_COLLABORATION_ERROR_SESSION_CLOSED
};

GQuark gedit_collaboration_error_quark (void);

void gedit_collaboration_get_sv (GtkWidget *widget,
                                 gdouble   *sat,
                                 gdouble   *val);

void gedit_collaboration_hue_to_rgba (gdouble hue, GdkRGBA *rgba);
gdouble gedit_collaboration_rgba_to_hue (GdkRGBA *rgba);

GtkBuilder *gedit_collaboration_create_builder (const gchar *resource);

gchar *gedit_collaboration_generate_new_name (const gchar *name,
                                              gint        *name_failed_counter);

G_END_DECLS

#endif /* __GEDIT_COLLABORATION_H__ */

