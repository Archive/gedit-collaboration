/*
 * Copyright (C) 2010 - Jesse van den Kieboom
 *
 * gedit-collaboration is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gedit-collaboration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gedit-collaboration. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gedit-collaboration.h"
#include <math.h>

GQuark
gedit_collaboration_error_quark (void)
{
	static GQuark quark = 0;

	if (G_UNLIKELY (quark == 0))
	{
		quark = g_quark_from_string ("gedit_collaboration_error");
	}

	return quark;
}

void
gedit_collaboration_get_sv (GtkWidget *widget,
                            gdouble   *sat,
                            gdouble   *val)
{
	GdkColor color;
	GtkStyle *style;
	gdouble r, g, b;
	gdouble h;

	style = gtk_widget_get_style (widget);
	color = style->base[gtk_widget_get_state (widget)];

	r = color.red / 65535.0;
	g = color.green / 65535.0;
	b = color.blue / 65535.0;

	gtk_rgb_to_hsv (r, g, b, &h, sat, val);

	*sat = *sat * 0.5 + 0.3;
	*val = (pow(*val + 1, 3) - 1) / 7 * 0.6 + 0.4;
}

void
gedit_collaboration_hue_to_rgba (gdouble  hue,
                                 GdkRGBA *rgba)
{
	gdouble r, g, b;

	gtk_hsv_to_rgb (hue, 0.5, 0.5, &r, &g, &b);

	rgba->red = r;
	rgba->green = g;
	rgba->blue = b;
	rgba->alpha = 1.0;
}

gdouble
gedit_collaboration_rgba_to_hue (GdkRGBA *rgba)
{
	gdouble h, s, v;

	gtk_rgb_to_hsv (rgba->red, rgba->green, rgba->blue, &h, &s, &v);

	return h;
}

GtkBuilder *
gedit_collaboration_create_builder (const gchar *resource)
{
	GtkBuilder *builder;
	gchar *full_resource;
	GError *error = NULL;

	builder = gtk_builder_new ();
	gtk_builder_set_translation_domain (builder, GETTEXT_PACKAGE);

	full_resource = g_strdup_printf ("/org/gnome/gedit/plugins/collaboration/ui/%s",
	                                 resource);

	if (!gtk_builder_add_from_resource (builder, full_resource, &error))
	{
		g_warning ("Could not construct builder for resource %s: %s",
		           resource,
		           error->message);

		g_error_free (error);
	}

	g_free (full_resource);

	return builder;
}

gchar *
gedit_collaboration_generate_new_name (const gchar *name,
                                       gint        *name_failed_counter)
{
	gchar *new_name;
	gchar *suffix;

	++(*name_failed_counter);

	suffix = g_strnfill (++(*name_failed_counter),
	                     '_');

	new_name = g_strdup_printf ("%s%s",
	                            name,
	                            suffix);

	g_free (suffix);

	return new_name;
}
