/*
 * Copyright (C) 2010 - Jesse van den Kieboom
 *
 * gedit-collaboration is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gedit-collaboration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gedit-collaboration. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GEDIT_COLLABORATION_ACTIONS_H__
#define __GEDIT_COLLABORATION_ACTIONS_H__

#include "gedit-collaboration-window-helper.h"

G_BEGIN_DECLS

void on_action_file_new (GtkAction                      *action,
                         GeditCollaborationWindowHelper *helper);

void on_action_folder_new (GtkAction                      *action,
                           GeditCollaborationWindowHelper *helper);

void on_action_item_delete (GtkAction                      *action,
                            GeditCollaborationWindowHelper *helper);

void on_action_bookmark_new (GtkAction                      *action,
                             GeditCollaborationWindowHelper *helper);

void on_action_bookmark_edit (GtkAction                      *action,
                              GeditCollaborationWindowHelper *helper);

void on_action_session_disconnect (GtkAction                      *action,
                                   GeditCollaborationWindowHelper *helper);

G_END_DECLS

#endif /* __GEDIT_COLLABORATION_ACTIONS_H__ */

