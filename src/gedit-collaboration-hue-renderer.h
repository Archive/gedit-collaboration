/*
 * Copyright (C) 2010 - Jesse van den Kieboom
 *
 * gedit-collaboration is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gedit-collaboration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gedit-collaboration. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GEDIT_COLLABORATION_HUE_RENDERER_H__
#define __GEDIT_COLLABORATION_HUE_RENDERER_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GEDIT_TYPE_COLLABORATION_HUE_RENDERER			(gedit_collaboration_hue_renderer_get_type ())
#define GEDIT_COLLABORATION_HUE_RENDERER(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_TYPE_COLLABORATION_HUE_RENDERER, GeditCollaborationHueRenderer))
#define GEDIT_COLLABORATION_HUE_RENDERER_CONST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_TYPE_COLLABORATION_HUE_RENDERER, GeditCollaborationHueRenderer const))
#define GEDIT_COLLABORATION_HUE_RENDERER_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GEDIT_TYPE_COLLABORATION_HUE_RENDERER, GeditCollaborationHueRendererClass))
#define GEDIT_IS_COLLABORATION_HUE_RENDERER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEDIT_TYPE_COLLABORATION_HUE_RENDERER))
#define GEDIT_IS_COLLABORATION_HUE_RENDERER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GEDIT_TYPE_COLLABORATION_HUE_RENDERER))
#define GEDIT_COLLABORATION_HUE_RENDERER_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS ((obj), GEDIT_TYPE_COLLABORATION_HUE_RENDERER, GeditCollaborationHueRendererClass))

typedef struct _GeditCollaborationHueRenderer		GeditCollaborationHueRenderer;
typedef struct _GeditCollaborationHueRendererClass	GeditCollaborationHueRendererClass;
typedef struct _GeditCollaborationHueRendererPrivate	GeditCollaborationHueRendererPrivate;

struct _GeditCollaborationHueRenderer
{
	GtkCellRenderer parent;

	GeditCollaborationHueRendererPrivate *priv;
};

struct _GeditCollaborationHueRendererClass
{
	GtkCellRendererClass parent_class;
};

GType gedit_collaboration_hue_renderer_get_type (void) G_GNUC_CONST;
void _gedit_collaboration_hue_renderer_register_type (GTypeModule *type_module);

GtkCellRenderer *gedit_collaboration_hue_renderer_new (void);

G_END_DECLS

#endif /* __GEDIT_COLLABORATION_HUE_RENDERER_H__ */
